/*
Code by Kiran George Vetteth, University of Pennsylvania 2015, 2/14/2015

This class provides the Abstraction for a Dealer behaviour.

@File Dealer.java:	Functions of a Dealer
*/

import java.io.*;
import java.util.*;
import java.lang.*;

public class Dealer
{
	private PlayingCard faceUp = new PlayingCard();
			PlayingCard faceDown = new PlayingCard();
			boolean showFaceDownCard;														//Hide FaceDown Card till Decision Moment
			int handCardValue;																// Current cards numerical value
			String name;
			int casinoMoneyGained;
			void playerDealerDisplay(Player bJackPlayer)
			{
				System.out.println(bJackPlayer.getName()+"\t\t\t\t\t\t\t|"+"Dealer:"+getName());
				System.out.println("Bet Amount:"+bJackPlayer.getBetAmount()+"\t\t"+"Left Lower Card:"+bJackPlayer.getLeftCard().rank+" "+bJackPlayer.getLeftCard().suit+"\t\t|"+"FaceUp Card:"+getFaceUpCard().rank+" "+getFaceUpCard().suit);
				System.out.println("Account Balance:"+bJackPlayer.getAccountBalance()+"\t"+"Right Upper Card:"+bJackPlayer.getRightCard().rank+" "+bJackPlayer.getRightCard().suit+"\t|"+"FaceDown Card:"+getFaceDownCard().rank+" "+getFaceDownCard().suit);
				System.out.println("\t\t\t"+"Hand Value:"+bJackPlayer.getCurrentCardValue()+"\t\t\t|"+"Hand Value:"+getCurrentCardValue());	
			}
			void hit(Deck gameDeck)															//During Stay condition and CardValue<16
			{
				PlayingCard card = new PlayingCard();
				card=gameDeck.getCard();
				faceDown=card;
				handCardValue+=gameDeck.getValueOf(faceDown);
			}
	public	Dealer(String dName)
			{
				name=dName;
				handCardValue=0;
				casinoMoneyGained=0;
				showFaceDownCard=false;
			}
			void addDeck(PlayingCard dCard, Deck gameDeck)									//Adds a card to the Dealers Pile
			{
				if(faceUp.suit == null)
				{
					faceUp=dCard;
				}
				else
					faceDown=dCard;				
				handCardValue=gameDeck.getValueOf(faceUp);	
			}
			void updateDealer(Player currPlayer, Deck gameDeck)								//Dealers response to Players Decisions
			{
				switch(currPlayer.action)
				{
					case HIT:		PlayingCard card = new PlayingCard();
									card=gameDeck.getCard();
									currPlayer.addCard(card, gameDeck);
									if(currPlayer.getCurrentCardValue() > 21)
									{
										showFaceDownCard=true;
										currPlayer.state=PlayerState.BUST;
										casinoMoneyGained+=currPlayer.getBetAmount();
										currPlayer.betAmountLost(currPlayer.getBetAmount());
									}
									else if(currPlayer.getCurrentCardValue() == 21)
										{
											showFaceDownCard=true;
											currPlayer.state=PlayerState.WIN;
											currPlayer.depositWinnings(currPlayer.getBetAmount()*2);
											currPlayer.depositWinnings(currPlayer.getBetAmount()/2);
											currPlayer.clearBetAmount();
										}
									currPlayer.setSideRuleDone();
									break;
					case STAY:		handCardValue+=gameDeck.getValueOf(faceDown);			//Add FaceDown Card to Dealers Total
									showFaceDownCard=true;
									while(handCardValue < 16)
									{
										playerDealerDisplay(currPlayer);
										hit(gameDeck);
									}	
									if(handCardValue > 21)
									{
										currPlayer.state=PlayerState.WIN;
										currPlayer.depositWinnings(currPlayer.getBetAmount()*2);
										currPlayer.clearBetAmount();
									}
									else if((handCardValue > currPlayer.getCurrentCardValue()) || (currPlayer.getCurrentCardValue() > 21))
									{
										currPlayer.state=PlayerState.BUST;
										casinoMoneyGained+=currPlayer.getBetAmount();
										currPlayer.betAmountLost(currPlayer.getBetAmount());
									}
									else if(handCardValue < currPlayer.getCurrentCardValue())
									{
										currPlayer.state=PlayerState.WIN;
										currPlayer.depositWinnings(currPlayer.getBetAmount()*2);
										currPlayer.clearBetAmount();
									} 
									else if(handCardValue == currPlayer.getCurrentCardValue())
									{
										currPlayer.state=PlayerState.PUSH;
										currPlayer.depositWinnings(currPlayer.getBetAmount());
										currPlayer.clearBetAmount();
									}
									currPlayer.setSideRuleDone();
									break;
					case DOUBLEDOWN:if(currPlayer.sideRuleUsed())
									{
										currPlayer.playerError=Error.SIDERULEUSED;
										break;
									}
									if(currPlayer.getDoubleDownBetAmount() > currPlayer.getBetAmount())
									{
										currPlayer.playerError=Error.DOUBLEEXCEED;
										break;
									}
									if(!currPlayer.addBetAmount(currPlayer.getDoubleDownBetAmount()))
										currPlayer.playerError=Error.BROKE;
									currPlayer.setSideRuleDone();	
									break;
					case SURRENDER:	if(currPlayer.sideRuleUsed())
									{
										currPlayer.playerError=Error.SIDERULEUSED;
										break;
									}
									handCardValue+=gameDeck.getValueOf(faceDown);			//Check for 21
									System.out.println("Dealer Value:"+handCardValue);	
									if(handCardValue == 21)
										if(handCardValue == currPlayer.getCurrentCardValue())
										{
											showFaceDownCard=true;
											currPlayer.state=PlayerState.PUSH;
											currPlayer.clearBetAmount();
										}
										else
										{
											showFaceDownCard=true;
											currPlayer.state=PlayerState.BUST;
											casinoMoneyGained+=currPlayer.getBetAmount();
											currPlayer.betAmountLost(currPlayer.getBetAmount());
										}
									else
										{		
											showFaceDownCard=true;
											currPlayer.state=PlayerState.BUST;
											casinoMoneyGained+=currPlayer.getBetAmount()/2;
											currPlayer.betAmountLost(currPlayer.getBetAmount()/2);
											currPlayer.depositWinnings(currPlayer.getBetAmount());
										}
									currPlayer.setSideRuleDone();	
									break;
					case SPLIT:		if(!currPlayer.getLeftCard().rank.equals(currPlayer.getRightCard().rank))
									{
										currPlayer.playerError=Error.SPLITSUNEQUEL;
										break;
									}
									if(currPlayer.getAccountBalance() < currPlayer.getBetAmount())
									{
										currPlayer.playerError=Error.BROKE;
										break;
									}
									currPlayer.splitCards(gameDeck);						//Player Splits, and continues with current Card
									currPlayer.addCard(gameDeck.getCard(), gameDeck);		//Refill missing card of current set
									break;
				}						
			}
			PlayingCard deal(Deck gameDeck)
			{
				return gameDeck.getCard();
			}
			PlayingCard getFaceUpCard()
			{
				return faceUp;
			}
			PlayingCard getFaceDownCard()
			{
				PlayingCard Card = new PlayingCard();
				if(showFaceDownCard)
					Card=faceDown;
				return Card;
			}
			String getName()
			{
				return name;
			}
			int getCurrentCardValue()
			{
				return handCardValue;
			}
}