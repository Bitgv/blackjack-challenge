public class PlayingCard
{
public	CardSuit suit;
		CardRank rank;
		PlayingCard()
		{
			suit=null;
			rank=null;
		}
		PlayingCard(CardSuit thisSuit, CardRank thisRank)
		{
			suit=thisSuit;
			rank=thisRank;
		}
		PlayingCard(PlayingCard Card)
		{
			suit=Card.suit;
			rank=Card.rank;
		}
}