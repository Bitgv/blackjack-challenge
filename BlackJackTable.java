/*
Code by Kiran George Vetteth, University of Pennsylvania 2015, 2/14/2015

This class provides the highest level of Black Jack Abstraction.
Designed for Ace = 11
TotalBetOnTable variable is reserved for future work, to encoperate multiple players

@File BlackJackTable.java:	BlackJack game Abstraction, implements the use of Dealer, Player and Deck Member functions
							Also uses PlayerState, PlayerOption and Error Member Variables to improve readability.
*/

import java.util.*;
import java.lang.*;
import java.io.*;

class BlackJackTable extends Deck
{
	private int totalBetOnTable;
	private static final int ACCTBALANCE = 100;
			void playerDisplay(Player bJackPlayer, Dealer bJackDealer)					//Display Function for User Interface
			{
				System.out.println(bJackPlayer.getName()+"\t\t\t\t\t\t\t|"+"Dealer:"+bJackDealer.getName());
				System.out.println("Bet Amount:"+bJackPlayer.getBetAmount()+"\t\t"+"Left Lower Card:"+bJackPlayer.getLeftCard().rank+" "+bJackPlayer.getLeftCard().suit+"\t|"+"FaceUp Card:"+bJackDealer.getFaceUpCard().rank+" "+bJackDealer.getFaceUpCard().suit);
				System.out.println("Account Balance:"+bJackPlayer.getAccountBalance()+"\t"+"Right Upper Card:"+bJackPlayer.getRightCard().rank+" "+bJackPlayer.getRightCard().suit+"\t|"+"FaceDown Card:"+bJackDealer.getFaceDownCard().rank+" "+bJackDealer.getFaceDownCard().suit);
				System.out.println("\t\t\t"+"Hand Value:"+bJackPlayer.getCurrentCardValue()+"\t\t\t|"+"Hand Value:"+bJackDealer.getCurrentCardValue());	
			}
	public	BlackJackTable()
			{
				totalBetOnTable=0;
			}
	public static void main (String[] args) throws java.lang.Exception
	{
		BlackJackTable table = new BlackJackTable();
		Deck gameDeck = new Deck();
		Scanner reader = new Scanner(System.in);
		String playerName;
		int playerBet=0, userInput=0;
		String userContinue;
		
		System.out.println("Hello!");
		System.out.println("Player Name:");
		playerName=reader.nextLine();
		Player bJackPlayer=new Player(ACCTBALANCE, playerName);							//Create an instance of Player with Initial Account Balance and Name
		while(bJackPlayer.getState() == PlayerState.PLAY)								//Game runs until Player wants to LEAVE
		{
			bJackPlayer.resetBets();													//Reset volatile Player parameters for new game
			Dealer bJackDealer = new Dealer("Lauren");
			if(bJackPlayer.getAccountBalance() == 0)
			{
				System.out.println("Account Balance Depleted");
				return;
			}
			System.out.println("Amount you want to Bet?\t\t"+"Account Balance:"+bJackPlayer.getAccountBalance());
			playerBet=reader.nextInt();
			while(!bJackPlayer.addBetAmount(playerBet))									//Error Check for BetAmount
			{
				System.out.println("Value Exceeds your Account!");
				System.out.println("Amount you want to Bet?");
				playerBet=reader.nextInt();
			}
			System.out.println("\nLets Play!!!");
			System.out.println("First Deal");
			bJackPlayer.addCard(bJackDealer.deal(gameDeck), gameDeck);					//Dealer Deals 1st(Left lower placed Card) to Player
			bJackPlayer.addCard(bJackDealer.deal(gameDeck), gameDeck);					//Dealer Deals 2nd(Right Upper placed Card) to Player
			bJackDealer.addDeck(gameDeck.getCard(), gameDeck);							//Dealer gets his/her Up faced card
			bJackDealer.addDeck(gameDeck.getCard(), gameDeck);							//Dealer gets his/her Down faced card
			table.playerDisplay(bJackPlayer, bJackDealer);
			
			while(bJackPlayer.getState() == PlayerState.PLAY)							// Current round continues until Player WINS, BUST, PUSH
			{
				System.out.println("\nWhat would you like to do?");
				System.out.println("-Enter the Equivalent numerical value");
				System.out.println("1.Hit \t\t 2.Stay");
				System.out.println("3.DoubleDown \t 4.Split \t 5.Surrender\n");
				userInput=reader.nextInt();
				switch(userInput)
				{
					case 1:	bJackPlayer.setAction(PlayerOption.HIT);
							System.out.println("HIT!");
							break;
					case 2:	bJackPlayer.setAction(PlayerOption.STAY);
							System.out.println("STAY!");
							break;					
					case 3:	bJackPlayer.setAction(PlayerOption.DOUBLEDOWN);
							System.out.println("DOUBLEDOWN!");
							System.out.println("Enter DoubleDown Amount\n");
							bJackPlayer.setDoubleDownBetAmount(reader.nextInt());
							bJackPlayer.setLastTurn(true);
							break;						
					case 4:	bJackPlayer.setAction(PlayerOption.SPLIT);
							System.out.println("SPLIT!");
							break;						
					case 5:	bJackPlayer.setAction(PlayerOption.SURRENDER);
							System.out.println("SURRENDER!");
							break;						
					default:System.out.println("-----Invalid Input. Please Try again-----");
							continue;
				}
				bJackDealer.updateDealer(bJackPlayer, gameDeck);							//Dealer takes action on user input
				table.playerDisplay(bJackPlayer, bJackDealer);
				switch(bJackPlayer.playerError)
				{
					case SIDERULEUSED:	System.out.println("Side Advantages Used Up!!");
										break;
					case DOUBLEEXCEED:	System.out.println("Double Down Amount > Bet Amount!!");
										break;	
					case BROKE:			System.out.println("Account Balance Cannot Cover Bet!!");
										break;						
					case SPLITSUNEQUEL:	System.out.println("Splitting on Unequel Cards not Alowed!!");
										break;																
				}
				if(bJackPlayer.getLastTurn())												// If DOUBLEDOWN, and last turn events
				{
					System.out.println("Last Card Dealt!!");
					bJackPlayer.addCard(bJackDealer.deal(gameDeck), gameDeck);
					bJackPlayer.setAction(PlayerOption.STAY);
					bJackDealer.updateDealer(bJackPlayer, gameDeck);
					table.playerDisplay(bJackPlayer, bJackDealer);
				}
			}
			switch(bJackPlayer.getState())
			{
				case WIN:		System.out.println("YOU WON!!!");
								break;
				case BUST:		System.out.println("BUSTED!!!");
								if(bJackPlayer.splitCardsPresent())
								{
									System.out.println("Using Previous Split Deck");
									bJackPlayer=bJackPlayer.getPreviousSplit();
								}
								break;
				case PUSH:		System.out.println("PUSH!!!");
								if(bJackPlayer.splitCardsPresent())
								{
									System.out.println("Using Previous Split Deck");
									bJackPlayer=bJackPlayer.getPreviousSplit();
								}
								break;			
			}
			System.out.println("Account Balance:"+bJackPlayer.getAccountBalance());
			System.out.println("Do you want to Play again(Yes/No)?");
			userContinue=reader.nextLine();
			userContinue=reader.nextLine();
			if(userContinue.equals("Yes"))
				bJackPlayer.updateState(PlayerState.PLAY);
			else
			{
				bJackPlayer.updateState(PlayerState.LEAVE);
				System.out.println("GoodBye");
			}
		}
	}
}