public enum PlayerState
{
	PUSH,
	BUST,
	WIN,
	PLAY,
	LEAVE,
	SURRENDER
}