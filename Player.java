/*
Code by Kiran George Vetteth, University of Pennsylvania 2015, 2/14/2015

This class provides the Abstraction for a Player behaviour.
Each Split card is entered into a LIFO Stack

@File Player.java:	Functions of a Player
*/

import java.io.*;
import java.util.*;
import java.lang.*;

public class Player
{
	private		Stack<Player> splitHand = new Stack<Player>();						//Each Split card is entered into a LIFO Stack
				
				static int acctBalance;												//Account Balance Shared amoung Split card bets
				int betAmount;
				int doubleDownBetAmount;
				
				PlayingCard cardLeft = new PlayingCard();
				PlayingCard cardRight = new PlayingCard();
				final PlayingCard nullCard = new PlayingCard();	
				int handCardValue;													//Current card numerical value
				
				PlayerState state;													//State of Player
				PlayerOption action; 												//Actions taken by player
				Error playerError;													//Record of rule violation
				boolean sideRuleDone;
				boolean lastTurn;
				
				String name;
				
				boolean haveMoney(int betAmt)
				{
					return (acctBalance >= betAmt);
				}
				void resetBets()													//A new round of Black Jack
				{
					clearBetAmount();
					cardLeft=nullCard;
					cardRight=nullCard;
					handCardValue=0;
					sideRuleDone = false;
					setLastTurn(false);
				}
	public		Player(PlayingCard pCard, int splitAmt, Deck gameDeck)				//Constructor for new split card set
				{
					acctBalance-=splitAmt;
					betAmount=splitAmt;
					cardLeft=pCard;
					handCardValue=gameDeck.getValueOf(pCard);
					state=PlayerState.PLAY;
					action=PlayerOption.SPLIT;
					sideRuleDone=false;
					lastTurn=false;
					playerError=Error.NONE;
				}
				Player(int acctBal, String myName)									//Constructor for player profile creation in game
				{
					acctBalance=acctBal;
					betAmount=0;
					handCardValue=0;
					state=PlayerState.PLAY;
					action=PlayerOption.NONE;
					sideRuleDone=false;
					lastTurn=false;
					playerError=Error.NONE;
					name=myName;	
				}
				void addCard(PlayingCard pCard, Deck gameDeck)						//Adds a card to the Players Deck
				{
					if(cardRight.suit == null)
						cardRight=pCard;
					else
						cardLeft=pCard;
					handCardValue+=gameDeck.getValueOf(pCard);	
				}
				boolean addBetAmount(int amount)
				{
					boolean permission=false;
					if(haveMoney(amount))
					{
						permission=true;
						acctBalance-=amount;
						betAmount+=amount;
					}
					return permission;
				}
				void splitCards(Deck gameDeck)										//Splits the cards if equal
				{
					Player splitPlayer=new Player(cardLeft, betAmount, gameDeck);
					handCardValue-=gameDeck.getValueOf(cardLeft);
					cardLeft=cardRight;
					cardRight=nullCard;
					splitHand.push(splitPlayer);
				}
				void updateState(PlayerState pState)
				{
					state=pState;
				}
				void setAction(PlayerOption option)
				{
					action=option;
				}
				void setSideRuleDone()
				{	
					sideRuleDone=true;
				}
				void depositWinnings(int amount)
				{
					acctBalance+=amount;
				}
				void betAmountLost(int amount)
				{
					betAmount-=amount;
				}
				void setDoubleDownBetAmount(int amt)
				{
					doubleDownBetAmount=amt;
				}
				void clearBetAmount()
				{
					betAmount=0;
				}
				void setLastTurn(boolean value)
				{
					lastTurn=value;
				}
				boolean getLastTurn()
				{
					return lastTurn;
				}
				boolean sideRuleUsed()
				{
					return sideRuleDone;
				}
				int getCurrentCardValue()
				{
					return handCardValue;
				}
				PlayerOption getAction()
				{
					return action;
				}
				PlayerState getState()
				{
					return state;
				}
				String getName()
				{
					return name;
				}
				int getAccountBalance()
				{
					return acctBalance;
				}
				boolean splitCardsPresent()
				{
					return !splitHand.empty();
				}
				Player getPreviousSplit()
				{
					return splitHand.pop();
				}
				int getDoubleDownBetAmount()
				{
					return doubleDownBetAmount;
				}
				int getBetAmount()
				{
					return betAmount;
				}
				PlayingCard getLeftCard()
				{
					return cardLeft;
				}
				PlayingCard getRightCard()
				{
					return cardRight;
				}
				Error getError()
				{
					return playerError;
				}
}