Blackjack Challenge
By Kiran George Vetteth

Description: 
Designed a BlackJack game interface, implementing standard BlackJack rules and side rules of DoubleDown, Split and Surrender

Installation:
Copy all files to a folder.
Run the following command to compile
javac BlackJackTable.java

Run the program
java BlackJackTable

Information
Ace is represented as 11.
Initial Account Balance of Player = 100 units
Bet amount can be set by Player

Authors
Kiran George Vetteth