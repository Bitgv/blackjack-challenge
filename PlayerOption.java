public enum PlayerOption
{
	HIT,
	STAY,
	DOUBLEDOWN,
	SPLIT,
	SURRENDER,
	NONE
}