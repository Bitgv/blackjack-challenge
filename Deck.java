/*
Code by Kiran George Vetteth, University of Pennsylvania 2015, 2/14/2015

This class provides the Abstraction for a Deck of cards.
Designed for Ace = 11
Deck of cards represented in a LIFO Stack

@File Deck.java:	Functions as a container for Playing cards.
					Can Shuffle Cards, to randomise Deck
					Can generate a random full Deck
					Provides an interface to get numerical value of Face cards
*/

import java.util.*;
import java.lang.*;

public class Deck
{
	private		static final int DECK_SIZE = 52;
	protected	Stack<PlayingCard> deckOfCards = new Stack<PlayingCard>();
	public		Deck()
				{ 
					shuffleDeck();
				}
				void shuffleDeck()
				{
					PlayingCard[] thisCard = new PlayingCard[DECK_SIZE];
					Random rand=new Random();
					int thisCardIndex=0, myRandNum=0;
					for(CardSuit thisSuit : CardSuit.values())					//Populate Array of 52 PlayingCards
					{
						for(CardRank thisRank : CardRank.values())
						{
							thisCard[thisCardIndex] = new PlayingCard(thisSuit,thisRank);
							++thisCardIndex;
						}	
					}
					thisCardIndex=0;
					while(thisCardIndex <= (DECK_SIZE-1))						//Randomly select Cards and push
					{
						myRandNum = rand.nextInt(DECK_SIZE);
						if(thisCard[myRandNum].suit != null)
						{
							deckOfCards.push(new PlayingCard(thisCard[myRandNum]));
							thisCard[myRandNum].suit = null;
							thisCard[myRandNum].rank = null;
							++thisCardIndex;
						}
					}
				}
				PlayingCard getCard()
				{
					if(deckOfCards.empty())
						shuffleDeck();
					return deckOfCards.pop();
				}
				boolean empty()
				{
					return deckOfCards.empty();
				}
				int getValueOf(PlayingCard card)
				{
					return card.rank.getVal();
				}
}
